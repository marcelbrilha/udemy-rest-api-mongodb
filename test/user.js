const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../dist/app').default.app

chai.use(chaiHttp)

const expect = chai.expect
const handleError = error => {
  const message = (error.message) ? error.response.res.text : error.message || error
  return Promise.reject(`${error.name}: ${message}`)
}

describe('User', () => {
  it('should create a new user', () => {
    const body = {
      name: 'mocha',
      email: 'mocha_default@gmail.com',
      password: '123'
    }

    return chai.request(app)
      .post('/api/users')
      .set('content-type', 'application/json')
      .send(JSON.stringify(body))
      .then(response => {
        expect(response.body).to.be.an('object')
        expect(response.body.result).to.be.equal('User created.')
      })
      .catch(handleError)
  })

  it('should return all users', () => {
    return chai.request(app)
      .get('/api/users')
      .set('content-type', 'application/json')
      .then(response => {
        expect(response.body.result).to.be.an('array')
      })
      .catch(handleError)
  })

  it('should return user per id', () => {
    return chai.request(app)
      .get('/api/users/5bc3bac078cd1956def7e017')
      .set('content-type', 'application/json')
      .then(response => {
        expect(response.body.result).to.be.an('object')
        expect(response.body.result.name).to.be.equal('mocha')
      })
      .catch(handleError)
  })

  it('should edit user', () => {
    const body = {
      name: 'mocha123'
    }

    return chai.request(app)
      .patch('/api/users/5bc3bac078cd1956def7e017')
      .set('content-type', 'application/json')
      .send(JSON.stringify(body))
      .then(response => {
        expect(response.body).to.be.an('object')
        expect(response.body.result).to.be.equal('User edited.')
      })
      .catch(handleError)
  })

  it('should remove user', () => {
    return chai.request(app)
      .delete('/api/users/5bc3bb3fc818e0576960cf12')
      .set('content-type', 'application/json')
      .then(response => {
        expect(response.body).to.be.an('object')
        expect(response.body.result).to.be.equal('User removed.')
      })
      .catch(handleError)
  })
})
