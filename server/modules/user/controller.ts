import UserRepository from './repository'
import * as httpStatus from 'http-status'

const sendResponse = (res, statusCode, data) => {
  res.status(statusCode).json({
    result: data
  })
}

class UserController {
  getAll (req, res) {
    UserRepository
      .getAll()
      .then(users => sendResponse(res, httpStatus.OK, users))
      .catch(error => {
        console.error(`Error in get all users: ${error.message}`)
        sendResponse(res, httpStatus.INTERNAL_SERVER_ERROR, 'Error in get all users.')
      })
  }

  getById (req, res) {
    const id = { _id: req.params.id }

    if (id._id.length !== 24) {
      sendResponse(res, httpStatus.BAD_REQUEST, 'Id is required. Length 24 caracters.')
    }

    UserRepository
      .getById(id)
      .then(user => sendResponse(res, httpStatus.OK, user))
      .catch(error => {
        console.error(`Error in get find by id user: ${error.message}`)
        sendResponse(res, httpStatus.INTERNAL_SERVER_ERROR, 'Error in get find by id user.')
      })
  }

  create (req, res) {
    const user = req.body

    UserRepository
      .create(user)
      .then(user => sendResponse(res, httpStatus.CREATED, 'User created.'))
      .catch(error => {
        console.error(`Error in create user: ${error.message}`)
        sendResponse(res, httpStatus.INTERNAL_SERVER_ERROR, 'Error in create user.')
      })
  }

  update (req, res) {
    const id = { _id: req.params.id }
    const user = req.body

    if (id._id.length !== 24) {
      sendResponse(res, httpStatus.BAD_REQUEST, 'Id is required. Length 24 caracters.')
    }

    UserRepository
      .update(id, user)
      .then(user => sendResponse(res, httpStatus.OK, 'User edited.'))
      .catch(error => {
        console.error(`Error in update user: ${error.message}`)
        sendResponse(res, httpStatus.INTERNAL_SERVER_ERROR, 'Error in update user.')
      })
  }

  delete (req, res) {
    const id = { _id: req.params.id }

    if (id._id.length !== 24) {
      sendResponse(res, httpStatus.BAD_REQUEST, 'Id is required. Length 24 caracters.')
    }

    UserRepository
      .remove(id)
      .then(result => sendResponse(res, httpStatus.OK, 'User removed.'))
      .catch(error => {
        console.error(`Error in remove user: ${error.message}`)
        sendResponse(res, httpStatus.INTERNAL_SERVER_ERROR, 'Error in remove user.')
      })
  }
}

export default new UserController()
