import App from './app'
import app from './app';

App.app.listen(5000, () => {
  console.log('Running in port 5000.')
})

process.once('SIGUSR2', () => App.closeDatabaseConnection('nodemon restart', () => process.kill(process.pid, 'SIGUSR2')))
process.on('SIGINT', () => app.closeDatabaseConnection('execution interrupted', () => process.exit(0)))
