import * as mongoose from 'mongoose'

class Database {
  private DB_URI = 'mongodb://127.0.0.1/ts-rest-api'
  private DB_CONNECTION

  constructor () {
    this.createConnection()
  }

  createConnection () {
    mongoose.connect(this.DB_URI, { useNewUrlParser: true })
    mongoose.set('useCreateIndex', true)
    this.logger(this.DB_URI)
  }

  logger (uri) {
    this.DB_CONNECTION = mongoose.connection
    this.DB_CONNECTION.on('connected', () => console.log(`Mongoose connected: ${uri}`))
    this.DB_CONNECTION.on('error', error => console.log(`Error in connection: ${error}`))
    this.DB_CONNECTION.on('disconnected', () => console.log(`Mongoose disconnected: ${uri}`))
  }

  closeConnection (message, callback) {
    this.DB_CONNECTION.close(() => {
      console.log(`Mongoose disconnected: ${message}`)
      callback()
    })
  }
}

export default new Database()
