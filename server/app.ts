import * as express from 'express'
import * as morgan from 'morgan'
import * as bodyparser from 'body-parser'
import * as compression from 'compression'
import * as helmet from 'helmet'
import * as cors from 'cors'
import Database from './config/db'
import UserController from './modules/user/controller'

class App {
  public app: express.Application
  private morgan: morgan.Morgan
  private bodyParser

  constructor () {
    this.app = express()
    this.middleware()
    this.routes()
  }

  closeDatabaseConnection (message, callback) {
    Database.closeConnection(message, callback)
  }

  middleware () {
    this.app.use(cors({
      origin: '*',
      methods: ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'],
      allowedHeaders: ['Content-Type', 'Authorization', 'Accept-Enconding'],
      preflightContinue: false,
      optionsSuccessStatus: 204
    }))
    this.app.use(compression())
    this.app.use(helmet())
    this.app.use(morgan('dev'))
    this.app.use(bodyparser.json())
    this.app.use(bodyparser.urlencoded({ extended: true }))
  }

  routes () {
    this.app.route('/api/users')
      .get(UserController.getAll)
      .post(UserController.create)

    this.app.route('/api/users/:id')
      .get(UserController.getById)
      .patch(UserController.update)
      .delete(UserController.delete)
  }
}

export default new App()
